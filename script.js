/*
  Ejercicio: Programa con objetos en JavaScript
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: Programa que lee un archivo csv, crea una estructura de Personas con los datos del archivo y la presenta en una etiqueta HTML específica
*/
function leerArchivoCSV(){
	// Leemos archivo csv
	var archivo = new XMLHttpRequest();
	archivo.open('GET', 'csv.txt', false);
	archivo.onreadystatechange = function(){
		// Verificamos archivo csv
		if(archivo.readyState === 4 && (archivo.status === 200 || archivo.status == 0)){
			var csv = archivo.responseText;
			// Creamos estructura de Personas con datos de archivo csv
			personas = crearPersonas(csv);
			// Presentamos estructura de Personas
			elemento = document.getElementById('personas');
			elemento.innerHTML = 'Nombre | Edad<br/>';
			for(i in personas)
				elemento.innerHTML +=  personas[i].datos() + '<br/>';
		}
	}
	archivo.send(null);
}
function crearPersonas(csv){
	// Creamos estructura de Personas con datos de archivo csv 
	var personas = [];
	var lineas = csv.split('\n');
	var titulos = lineas[0].split(",");
	// Obtenemos datos de archivo csv
	for(i in lineas){
		if(i > 0){
			var linea = lineas[i].split(",");
			var nombre = linea[0];
			var edad = linea[1];
			// Creamos objeto para estructura de Personas
			var persona = new Persona(nombre, edad);
			// Agregamos objeto a estructura de Personas
			personas.push(persona);
		}
	}
	return personas;
}
function Persona(nombre, edad){
	// Definición de clase persona 
	this.nombre = nombre;
	this.edad = edad;
	this.datos = function(){return this.nombre + ' | ' + this.edad;};
}